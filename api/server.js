// Koa libraries
import Koa from "koa";
import KoaRouter from "koa-router";
import KoaBodyParser from "koa-bodyparser";
import Cors from '@koa/cors';

// Apollo Server for Koa
import { graphqlKoa, graphiqlKoa } from "apollo-server-koa";

// Prisma
import { Prisma } from "prisma-binding";

// GraphQL Playground Middleware for Koa
const koaPlayground = require("graphql-playground-middleware-koa").default;

// Schema and Resolvers
import { makeExecutableSchema } from "graphql-tools";
import { importSchema } from "graphql-import";
import Resolvers from "./graphql/resolvers";
const typeDefs = importSchema("./graphql/schema.graphql");

const PORT = process.env.API_PORT || 8080;
const NODE_ENV = process.env.NODE_ENV || "development";

const graphQLServer = new Koa();
const router = new KoaRouter();
const bodyParser = new KoaBodyParser();

// Validate GraphQL API Schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers: Resolvers,
  resolverValidationOptions: {
    requireResolversForResolveType: false
  }
});

// Use bodyparser middleware
graphQLServer.use(bodyParser);
graphQLServer.use(Cors());

// Define GraphQL endpoints
router.post(
  "/graphql",
  KoaBodyParser(),
  (ctx) => graphqlKoa({
    schema,
    context: {
      request: ctx.request,
      response: ctx.response,
      prisma: new Prisma({
        typeDefs: "graphql/generated/prisma.graphql",
        endpoint: "http://prisma:4466"
      })
    }
  })(ctx)
);

router.get(
  "/graphql",
  graphqlKoa({
    schema,
    context: () => ({
      prisma: new Prisma({
        typeDefs: "graphql/generated/prisma.graphql",
        endpoint: "http://prisma:4466"
      })
    })
  })
);

// Define endpoint for GraphQL Playground
router.all(
  "/playground",
  koaPlayground({
    endpoint: "/graphql"
  })
);

// Define GraphiQL endpoints
if (NODE_ENV !== "production") {
  router.get("/graphiql", graphiqlKoa({ endpointURL: "/graphql" }));
}

// Use router middleware
graphQLServer.use(router.routes());
graphQLServer.use(router.allowedMethods());

graphQLServer.listen(PORT, () => {
  console.log(
    `GraphQL Playground is now running on http://localhost:${PORT}/playground`
  );
});
